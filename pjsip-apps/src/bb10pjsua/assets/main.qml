import bb.cascades 1.0

TabbedPane {
    showTabsOnActionBar: true
    Tab {
        title: qsTr("Main App - With Libs?")
        Page {
            id: tab1
            actions: [
                //-- define the actions for first tab here
                ActionItem {
                    title: qsTr("Rotate")
                    onTriggered: {
                        imgTab1.rotationZ = imgTab1.rotationZ + 90;
                    }
                },
                ActionItem {
                    title: qsTr("Break")
                    onTriggered: {
                        imgTab1.imageSource = "asset:///images/picture1br.png";
                    }
                }
            ]
            Container {
                //-- define tab content here
                layout: StackLayout {
                }
                Label {
                    layoutProperties: StackLayoutProperties {
                        horizontalAlignment: HorizontalAlignment.Center
                    }
                    text: qsTr("Main App")
                    textStyle {
                        base: SystemDefaults.TextStyles.TitleText
                    }
                }
                TextField {
                        layoutProperties: StackLayoutProperties {
                            horizontalAlignment: HorizontalAlignment.Center
                        }
                        id: commands
                        inputMode: TextFieldInputMode.Text
                        textStyle {
                            base: SystemDefaults.TextStyles.SubtitleText
                        }
                    }
                Button{
                    id: commandsubmit
                    text: "Submit"
                }
                TextArea{
                    id: response
                    inputMode: TextAreaInputMode.Text
                    minWidth: 5.0
                    minHeight: 15.0
                }
                ImageView {
                    id: imgTab1
                    imageSource: "asset:///images/picture1.png"
                    layoutProperties: StackLayoutProperties {
                        horizontalAlignment: HorizontalAlignment.Center
                        spaceQuota: 1.0
                        verticalAlignment: VerticalAlignment.Center
                    }
                    scalingMethod: ScalingMethod.AspectFit
                }
            }
        }
    }
    Tab {
        title: qsTr("Config")
        Page {
            id: tab2
            actions: [ 
            //-- define the actions for tab here
            ActionItem {
                    title: qsTr("Raise")
                    onTriggered: {
                        //-- run the image animation
                        raiseAnimation.play();
                    }
                }
            ]
            Container {
                //-- define tab content here
                layout: StackLayout  {
                }
                Label {
                    layoutProperties: StackLayoutProperties {
                        horizontalAlignment: HorizontalAlignment.Center
                    }
                    text: qsTr("Config")
                    textStyle {
                        base: SystemDefaults.TextStyles.TitleText
                    }
                }
                ImageView {
                    id: imgTab2
                    imageSource: "asset:///images/picture1.png"
                    layoutProperties: StackLayoutProperties {
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                        spaceQuota: 1.0
                    }
                    scalingMethod: ScalingMethod.AspectFit
                    opacity: 0.2
                    animations: [
                        //-- define animations for image here
                        FadeTransition {id: raiseAnimation; fromOpacity: 0.2; toOpacity: 1; duration: 1000}                                         
                    ]
                }
            }
        }
    }
}
