TEMPLATE = app
TARGET = bb10pjsua

CONFIG += qt warn_on debug_and_release cascades

QMAKE_CXXFLAGS += -DPJ_AUTOCONF -DUSE_GUI -Wp,-isystem,$(BOOST)

QMAKE_CFLAGS += -DPJ_AUTOCONF -DUSE_GUI

QMAKE_LDFLAGS +=


QMAKE_CXX = $$(CXX) -lang-c++
QMAKE_CC = $$(CC)
QMAKE_LINK = $$(LD) -lang-c++


device {
	CONFIG(release, debug|release) {
		DESTDIR = o.le-v7
	}
	CONFIG(debug, debug|release) {
		DESTDIR = o.le-v7-g
	}
	MACH = arm-unknown-nto-qnx8.0.0eabi
}

simulator {
	CONFIG(release, debug|release) {
		DESTDIR = o
	}
	CONFIG(debug, debug|release) {
		DESTDIR = o-g
	}
	MACH = i486-pc-nto-qnx8.0.0
}
CCVER = 4.6.3


INCLUDEPATH += ../src \
	$(PJSIP)/$${MACH}/pjlib/include \
	$(PJSIP)/$${MACH}/pjsip-apps/src/pjsua \
	$(PJSIP)/$${MACH}/pjlib-util/include \
	$(PJSIP)/$${MACH}/pjmedia/include \
	$(PJSIP)/$${MACH}/pjnath/include \
	$(PJSIP)/$${MACH}/pjsip/include \
	$(GMP)/$${MACH} $(GMP) \
	$(LIBMIKEYSAKKE)/include \

SOURCES += ../src/app.cpp ../src/main.cpp ../src/pjsua_app.c
HEADERS += ../src/app.hpp
LIBS += \
   -Bstatic \
   -L$(PJSIP)/$${MACH}/pjsip/lib \
   -L$(PJSIP)/$${MACH}/pjlib-util/lib \
   -L$(PJSIP)/$${MACH}/pjlib/lib \
   -L$(PJSIP)/$${MACH}/pjnath/lib \
   -L$(PJSIP)/$${MACH}/pjmedia/lib \
   -L$(PJSIP)/$${MACH}/third_party/lib \
   -L$(OPENCOREAMR)/$${MACH}/amrnb/.libs \
   -L$(GMP)/$${MACH}/.libs \
   -L$(LIBMIKEYSAKKE)/$${MACH}/lib \
   -L$(BOOST)/$${MACH}-stage/lib \
   -lpjsua-$${MACH} \
   -lpjsip-ua-$${MACH} \
   -lpjsip-simple-$${MACH} \
   -lpjsdp-$${MACH} \
   -lpjnath-$${MACH} \
   -lpjsip-$${MACH} \
   -lpjmedia-audiodev-$${MACH} \
   -lpjmedia-codec-$${MACH} \
   -lpjmedia-videodev-$${MACH} \
   -lpjmedia-$${MACH} \
   -lpjlib-util-$${MACH} \
   -lpj-$${MACH} \
   -lsrtp-$${MACH} \
   -lg7221codec-$${MACH} \
   -lgsmcodec-$${MACH} \
   -lilbccodec-$${MACH} \
   -lmilenage-$${MACH} \
   -lresample-$${MACH} \
   -lspeex-$${MACH} \
   -lopencore-amrnb \
   -lmikeysakke4c \
   -lmikeysakke \
   -lgmp \
   -Bdynamic \
   -lsocket \
   -laudio_manager \
   -lasound \
   -lcurl \
   -lboost_filesystem \
   -lboost_system \
   -lcrypto\
   -lssl \
   -linput_client \
   -linput_utilities \
   -lbbplatform \
   -lbbsystem \
   -lcpp


lupdate_inclusion {
    SOURCES += ../assets/*.qml
}

OBJECTS_DIR = $${DESTDIR}/.obj
MOC_DIR = $${DESTDIR}/.moc
RCC_DIR = $${DESTDIR}/.rcc
UI_DIR = $${DESTDIR}/.ui

TRANSLATIONS = \
    $${TARGET}_en_GB.ts \
    $${TARGET}_fr.ts \
    $${TARGET}_it.ts \    
    $${TARGET}_de.ts \
    $${TARGET}_es.ts \
    $${TARGET}.ts
