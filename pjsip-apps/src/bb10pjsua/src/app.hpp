#ifndef APP_H
#define APP_H

#include <QObject>
#include <bb/cascades/Application>
#include <bb/cascades/Button>
#include <bb/cascades/TextField>
#include <bb/cascades/TextArea>
#include <bb/cascades/ScrollView>
#include <bb/cascades/TabbedPane>
#include <string>
#include <QString>
#include <QDebug>
#include <list>
#include <bb/cascades/CustomControl>
#include<queue>
#include <map>
#include <bb/cascades/Option>
#include<bb/cascades/DropDown>

namespace bb
{
	namespace cascades
	{
		class TouchEvent;
	}
}


using namespace bb::cascades;

typedef std::map<std::list<std::string>, Option*> commandMapType;
typedef std::pair<std::list<std::string>, Option*> commandMapPair;

/*!
 * @brief Application GUI object
 */
class App : public bb::cascades::Application
{
	Q_OBJECT

public:
     App(int argc, char** argv);
     static App* instance() { return static_cast<App*>(Application::instance()); }
     void main();
     char * getInput(char *s, int n, FILE *stream);
	 void DisplayMsg( std::string& str );
	 void NewCallAlert();
	 void UpdateLog();

signals:
	 void CommandReceived();

public slots:
 	 void SaveConfigFile();
 	 void RevertConfigChanges();
 	 void ConstructAndForwardCommand();
 	 void ReloadProgram();
 	 void ClearOutput();
 	 void CommandSent( bb::cascades::Option* );

private:
    // components
    TabbedPane* tabbedPane;
	TextArea* configView;
	ScrollView* outputViewScroller;
    Button* sendCommandBtn;
    Button* placeCallBtn;
	Button* hangUpBtn;
	Button* quitBtn;
    TextField* commands;
    TextArea* outputLogView;
    DropDown* commandsDropDown;
    std::queue<std::string> commandQueue;
    commandMapType commandMap;

	//private methods
	void answer_call();
	void start_app();
};

#endif // ifndef APP_H
