#include <app.hpp>
#include <bb/cascades/Page>
#include <bb/cascades/ActionItem>
#include <bb/cascades/Container>
#include <QDebug>
#include<bb/cascades/TextStyle>
#include<bb/cascades/SystemDefaults>
#include<bb/cascades/TextFieldInputMode>
#include<bb/cascades/OrientationSupport>
#include<bb/cascades/SupportedDisplayOrientation>
#include<bb/cascades/TouchPropagationMode>
#include<bb/cascades/StackLayout>
#include<bb/cascades/StackLayoutProperties>
#include<bb/cascades/HorizontalAlignment>
#include<bb/cascades/LayoutOrientation>
#include<bb/cascades/OrientationSupport>
#include<bb/cascades/ScrollAnimation>
#include<bb/cascades/Paint>
#include<QMutex>
#include <boost/assign/list_of.hpp>

using namespace bb::cascades;

namespace
{
	// config file manipulation
	const char* CONFIG_FILE = "./app/native/assets/files/config.cfg";
	QFile configFileHandle(CONFIG_FILE);
	QByteArray currentConfig;

	// stores all the information printed from PJSip.
	std::string logger;
	// protect access to the logger.
	QMutex loggerAccess;
}

/* ************* Start of PJSip Specific Includes etc ************* */
// Bring in the pjsip header files that we need.
extern "C"
{
	//// PJSip Add ins
	#include <pjlib.h>
	#include <pjsua.h>
}

// Sleep interval duration
#define SLEEP_INTERVAL	    0.5
// Determine whether we should print the messages in the debugger console as well
#define DEBUGGER_PRINT	    1
// Whether we should show pj log messages in the text area
#define SHOW_LOG	    0
#define PATH_LENGTH	    PJ_MAXPATH
#define KEEP_ALIVE_INTERVAL 600

// Extern some pjsip variables
extern "C" pj_log_func *log_cb;
extern "C" pj_bool_t app_restart;
extern "C" pj_bool_t cmd_echo;

// Pjsip specific variables
bool			 app_running;
pj_thread_desc   a_thread_desc;
pj_thread_t		 *a_thread;
pjsua_call_id    ccall_id;

// Extern some pjsip methods we will be calling into
extern "C" pj_status_t app_init(int argc, char *argv[]);
extern "C" pj_status_t app_main(void);
extern "C" pj_status_t app_destroy(void);
extern "C" pj_status_t pjsua_acc_get_config(pjsua_acc_id acc_id, pjsua_acc_config *acc_cfg);
extern "C" pjsua_acc_id pjsua_acc_get_default(void);

/* ************* End of PJSip Includes etc.. ************* */


App::App(int argc, char** argv)
  : Application(argc, argv)
{
	typedef std::map<std::string, std::string> TStrStrMap;
	// Controls to drive the main app
	commands = TextField::create().hintText( "Enter Command or Number to Dial" );
	commands->setMinWidth(520);
	commandsDropDown = DropDown::create().title("Select Command");

	commandMap.insert(commandMapPair( boost::assign::list_of(""), Option::create().text( "Reset UI" )));
	commandMap.insert(commandMapPair( boost::assign::list_of("a")("200"), Option::create().text( "Answer Call" )));
	commandMap.insert(commandMapPair( boost::assign::list_of("h"), Option::create().text( "Hang Up" )));
	commandMap.insert(commandMapPair( boost::assign::list_of("r")("r"), Option::create().text( "Reload" )));

	for ( commandMapType::iterator it = commandMap.begin(); it != commandMap.end(); ++it  )
	{
		commandsDropDown->add( it->second );
	}

	QObject::connect( commandsDropDown, SIGNAL ( selectedOptionChanged( bb::cascades::Option* ) ), this, SLOT ( CommandSent( bb::cascades::Option* ) ) );

	placeCallBtn = Button::create().image(Image("asset:///images/phone.png"))
										.connect( SIGNAL ( clicked() ), this, SLOT ( ConstructAndForwardCommand() ) );
	hangUpBtn = Button::create().image(Image("asset:///images/hangUp.png"))
										.connect( SIGNAL ( clicked() ), this, SLOT ( ConstructAndForwardCommand() ) );
	sendCommandBtn = Button::create("Send Command")
										.connect( SIGNAL ( clicked() ), this, SLOT ( ConstructAndForwardCommand() ) );
	Button* clearOutputBtn = Button::create("Clear Output")
										.connect( SIGNAL ( clicked() ), this, SLOT ( ClearOutput() ) );;
	quitBtn = Button::create("Exit").connect( SIGNAL ( clicked() ), this, SLOT ( ConstructAndForwardCommand() ) );

	TextStyle *blueStyle = new TextStyle(SystemDefaults::TextStyles::smallText());
	blueStyle->setColor(Color::Blue);
	outputLogView = TextArea::create().textStyle( *( new TextStyle( SystemDefaults::TextStyles::smallText() ) ) ).editable( false );
	commands->textStyle()->setBase(*blueStyle);
	//outputLogView->textStyle()->setBase( *blueStyle );
	outputViewScroller = ScrollView::create().scrollMode( ScrollMode::Vertical ).content( outputLogView );
	outputViewScroller->setMaxHeight(800);

	// controls for modifying the configuration
	Button* saveBtn = Button::create("Save")
								.connect( SIGNAL ( clicked() ), this, SLOT ( SaveConfigFile() ) );
	Button* saveReloadBtn = Button::create("Save and Reload")
										.connect( SIGNAL ( clicked() ), this, SLOT ( ReloadProgram() ) );
	Button* revertBtn = Button::create("Revert")
									.connect( SIGNAL ( clicked() ), this, SLOT ( RevertConfigChanges() ) );
	configView = TextArea::create().textStyle( *( new TextStyle( SystemDefaults::TextStyles::smallText() ) ) );
	ScrollView* configViewScroller = ScrollView::create().scrollMode( ScrollMode::Vertical ).content( configView );
	configViewScroller->setMaxHeight(800);

	OrientationSupport::instance()->setSupportedDisplayOrientation( SupportedDisplayOrientation::DisplayPortrait);

	// read current configuration in, and display to user.
	if ( configFileHandle.open( QIODevice::ReadOnly | QIODevice::Text ) )
	{
		currentConfig = configFileHandle.readAll();
		configView->setText( currentConfig );
		configFileHandle.close();
	}

	// create the views for the main page
	Page* mainAppPage = Page::create().addAction( ActionItem::create().title("Main App") );
	Container* drivingButtons = Container::create().layout( StackLayout::create().orientation( LayoutOrientation::RightToLeft ) )
													   .add( quitBtn )
				                                       .add( commandsDropDown );
	Container* callButtons = Container::create().layout( StackLayout::create().orientation( LayoutOrientation::RightToLeft ) )
												   .add( hangUpBtn )
												   .add( placeCallBtn )
			                                       .add( commands );
	Container* otherCommandButtons = Container::create().layout( StackLayout::create().orientation( LayoutOrientation::RightToLeft ) )
												   .add( clearOutputBtn )
												   .add( sendCommandBtn );
	Container* mainContent = Container::create().layout( StackLayout::create().orientation( LayoutOrientation::TopToBottom ) )
												   .add( drivingButtons )
												   .add( callButtons )
												   .add( otherCommandButtons )
												   .add( outputViewScroller );

	mainAppPage->setContent( mainContent );

	// create the views for the config page
	Page* configPage = Page::create().addAction( ActionItem::create().title("Config") );
	configPage->setContent( Container::create().add( configViewScroller )
											   .add( Container::create().layout( StackLayout::create().orientation( LayoutOrientation::LeftToRight ) )
																		.add( saveBtn ).add( revertBtn ).add( saveReloadBtn ) ));

	Container* about = Container::create().add( TextArea::create().text( "PJSIP user agent shell for iPhone" ) );
	Page* aboutPage = Page::create().addAction( ActionItem::create().title("About") ).content( about );

	// now add all our content in
	tabbedPane = TabbedPane::create().showTabsOnActionBar(true).sidebarState(SidebarState::Hidden)
								.add( Tab::create().title("Main App").content( mainAppPage ) )
								.add( Tab::create().title("Config").content( configPage ) )
								.add( Tab::create().title("About").content( aboutPage ) );

	// we are setting what we are showing, our tabbedPane contains everything.
	Application::instance()->setScene( tabbedPane );
}

void App::ClearOutput()
{
	loggerAccess.lock();
	logger.clear();
	outputLogView->setText("");
	loggerAccess.unlock();
}

void App::CommandSent( bb::cascades::Option* selectedCommand )
{
	for ( commandMapType::iterator it = commandMap.begin(); it != commandMap.end(); ++it  )
	{
		if ( it->second == selectedCommand )
		{
			for ( std::list<std::string>::const_iterator c_it = it->first.begin(); c_it!= it->first.end(); ++c_it )
			{
				std::string command ( *(c_it)  );
				DisplayMsg( command );
				commandQueue.push( command );
			}

			emit CommandReceived();
			break;
		}
	}
}

extern "C" void showMsg(const char *format, ...)
{
#if DEBUGGER_PRINT
	qDebug() << "*****************showMsg******************";
#endif

	int bufferSize = 1024;
	// list of arguments
	va_list arg;
	// get first 'format' argument
    va_start(arg, format);

    // Is this what we really have to do to get this to work?!?
    char buffer[bufferSize];
    int requiredSize= vsnprintf( &buffer[0], bufferSize, format, arg );

    loggerAccess.lock();

    if ( requiredSize < bufferSize)
    {
    	logger+= std::string ( buffer );
    }
    else
    {
    	std::vector<char> buffer;
    	bufferSize = requiredSize;
    	buffer.resize(requiredSize);
    	requiredSize = vsnprintf( &buffer[0], bufferSize, format, arg );
    	logger+= std::string ( buffer.begin(), buffer.end() );
    }

#if DEBUGGER_PRINT
    qDebug() << QString::fromStdString( logger );
#endif

    loggerAccess.unlock();

    va_end(arg);

    App::instance()->UpdateLog();
}

void App::UpdateLog()
{
    emit CommandReceived();
}

extern "C" char* getInput(char *s, int n, FILE *stream)
{
	return App::instance()->getInput(s,n,stream);
}
char* App::getInput(char *s, int n, FILE *stream)
{
#if DEBUGGER_PRINT
	qDebug() << "*****************GETINPUT******************";
#endif

	// I need this for the config file, that query's this.
	if (stream != stdin)
	{

#if DEBUGGER_PRINT
		qDebug() << QString::fromStdString("stdin");
#endif
		return fgets( s, n, stream );
	}

	while (commandQueue.empty())
	{
		while ( commandQueue.empty() && logger.empty() )
		{
			// create a loop to block waiting on the clicked signal, this indicates we have a new command.
			QEventLoop loop;
			loop.connect( this, SIGNAL ( CommandReceived() ), SLOT ( quit() ) );
			loop.exec();
		}

		loggerAccess.lock();

		if ( !logger.empty() )
		{
			this->DisplayMsg( logger );
			// logged it, now free it up.
			logger = "";
		}

		loggerAccess.unlock();
	}

#if DEBUGGER_PRINT
	qDebug() << "*****************Input Received!******************";
	qDebug() << QString::fromStdString( commandQueue.front() );
#endif

	strncpy( s, commandQueue.front().c_str(), n );
	this->DisplayMsg( App::commandQueue.front() );
	commandQueue.pop();
	return s;
}

void showLog(int level, const char *data, int len)
{
#if DEBUGGER_PRINT
	qDebug() << "*****************showLog******************";
#endif
    showMsg("%s", data);
}

extern "C" pj_bool_t showNotification(pjsua_call_id call_id)
{
	ccall_id = call_id;
    App::instance()->NewCallAlert();
    return PJ_FALSE;
}

void App::NewCallAlert()
{
#if DEBUGGER_PRINT
	qDebug() << "/n/n********** Incoming Call Received **********/n/n";
#endif
	showMsg("Incoming Call....");
}

void App::ConstructAndForwardCommand()
{
#if DEBUGGER_PRINT
	qDebug() << "/n/n********** called **********/n/n";
#endif
	// can use this to compare pointers, instead of this elaborate panel thing.
	QObject::sender();
	if ( Button* btn = dynamic_cast<Button*>( QObject::sender() ) )
	{
		// i can cast this to a button and get the id, as i know it must be a button calling this.
		if ( btn == placeCallBtn )
		{
#if DEBUGGER_PRINT
			qDebug() << "in place call";
#endif
			// now get hold of the sip registrar
			pjsua_acc_config currentAccount;
			pjsua_acc_get_config( pjsua_acc_get_default(), &currentAccount );
			pj_str_t registration = currentAccount.reg_uri;

			std::string callString (registration.ptr, registration.slen);

#if DEBUGGER_PRINT
			qDebug() << "Registra URI: " << QString::fromStdString( callString );
#endif
			// find where the separator is and append the dialed number.
			callString.insert( (callString.find(":") + 1 ), App::commands->text().toUtf8() + "@"  );
			// send the command 'm' we are making a call, and the number to call.
			commandQueue.push( "m" );
			commandQueue.push( callString);
		}
		else if ( btn == hangUpBtn )
		{
#if DEBUGGER_PRINT
			qDebug() << "in hang up";
#endif
			// h is the command line key for hanging up a call.
			commandQueue.push( "h" );
		}
		else if ( btn == quitBtn )
		{
			commandQueue.push("q");
			emit CommandReceived();
			Application::instance()->quit();
		}
		else if ( btn == sendCommandBtn )
		{
#if DEBUGGER_PRINT
			qDebug() << "in other commands";
#endif
			commandQueue.push( std::string ( App::commands->text().toUtf8() ) );
		}

		emit CommandReceived();
	}
}

void App::answer_call()
{
    pjsua_call_answer( ccall_id, 200, NULL, NULL );
    showMsg("Accepting call...");
}

void App::main()
{

#if SHOW_LOG
    pj_log_set_log_func(&showLog);
    log_cb = &showLog;
#endif

    char *argv[] = { " ", "--config-file", "./app/native/assets/files/config.cfg"};

    do
    {
    	app_restart = PJ_FALSE;
		if ( app_init(3, argv) != PJ_SUCCESS )
		{
			std::string errorMessage = "Failed To Initialise PJSua :-(";
			DisplayMsg( errorMessage );
			outputLogView->setText("PJSip failed to load :-( ");
#if DEBUGGER_PRINT
			qDebug() << "*****************Application Failed******************";
#endif
		}
		else
		{
#if DEBUGGER_PRINT
			qDebug() << "*****************Application Launched******************";
#endif
			app_running = true;
			app_main();

			app_destroy();
			/* This is on purpose */
			app_destroy();
		}

		loggerAccess.lock();
		App::DisplayMsg( logger );
		loggerAccess.unlock();
    }
    while (app_restart);
}

/* *** END OF PJSIP SUFF *** */

void App::SaveConfigFile()
{
	QByteArray newConfig = configView->text().toLocal8Bit();
	if ( currentConfig != newConfig )
	{
		configFileHandle.open( QIODevice::WriteOnly );
		configFileHandle.write( newConfig );
		configFileHandle.close();
	}
	currentConfig = newConfig;
}

void App::RevertConfigChanges()
{
	configView->setText( currentConfig );
}

void App::ReloadProgram()
{
	// could call app_restart = false directly.
	commandQueue.push( "L" );
}

void App::DisplayMsg(std::string& str)
{
#if DEBUGGER_PRINT
	qDebug() << "***********************************";
	qDebug() << QString::fromStdString(str);
#endif
	QString display = outputLogView->text() + "\n" + QString::fromStdString(str);
	App::outputLogView->setText( display );
	outputViewScroller->scrollToPoint( 30000, 30000, ScrollAnimation::Smooth );
}
